import { createAppContainer } from 'react-navigation'
import { URL, URLSearchParams } from 'whatwg-url'
import { Buffer } from 'buffer'
import { Provider as PaperProvider } from 'react-native-paper'
import { Provider as StoreProvider } from 'react-redux'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import { createLogger } from 'redux-logger'
import { combineEpics, createEpicMiddleware } from 'redux-observable'
import 'rxjs'
import React from 'react'
import { allBalancesReducer, allBalancesRequestEpic } from './src/redux/allBalances'
import BottomNavigator from './src/containers/Navigator'

// react-native 0.59 add own global URLSearchParams without implementation
// https://github.com/facebook/react-native/blob/e6057095adfdc77ccbbff1c97b1e86b06dae340b/Libraries/Blob/URL.js#L66
global.Buffer = Buffer
global.URL = URL
global.URLSearchParams = URLSearchParams

const logger = createLogger({ collapsed: true })
const middleware = []
const initialState = {}
const epicMiddleware = createEpicMiddleware({
  dependencies: {},
})
middleware.push(epicMiddleware)
middleware.push(logger)

const store = createStore(
  combineReducers({ allBalancesReducer }),
  initialState,
  applyMiddleware(...middleware),
)
epicMiddleware.run(combineEpics(allBalancesRequestEpic))

const Navigator = createAppContainer(BottomNavigator)

export default class App extends React.Component {
  render() {
    return (
      <StoreProvider store={store}>
        <PaperProvider>
          <Navigator />
        </PaperProvider>
      </StoreProvider>
    )
  }
}
