Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _reactNative = require('react-native')

const STATUSBAR_HEIGHT = _reactNative.Platform.OS === 'ios' ? 20 : _reactNative.StatusBar.currentHeight + 15; const _default = _reactNative.StyleSheet.create({
  statusBar: {
    color: 'black',
    height: STATUSBAR_HEIGHT
  }
}); exports.default = _default
