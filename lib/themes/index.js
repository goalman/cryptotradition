Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _reactNative = require('react-native')

const _default = _reactNative.StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: 'black'
  },

  item: {
    alignItems: 'flex-start',
    fontSize: 30,
    color: 'black'
  }
}); exports.default = _default
