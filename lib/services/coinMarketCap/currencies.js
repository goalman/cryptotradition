const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _httpClient = _interopRequireDefault(require('../httpClient'))
const _config = _interopRequireDefault(require('../config'))

const

  { request } = _httpClient.default


function getCurrencyLatestData(params) {
  return request({
    url: '/v1/cryptocurrency/listings/latest',
    method: 'GET',
    baseURL: _config.default.coinMarketCap.url,
    headers: { 'X-CMC_PRO_API_KEY': _config.default.coinMarketCap.apiKey },

    params
  })
} const _default = getCurrencyLatestData; exports.default = _default
