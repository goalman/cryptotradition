const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _httpClient = _interopRequireDefault(require('../httpClient'))
const _config = _interopRequireDefault(require('../config'))

const

  { request } = _httpClient.default


function getBalances(params) {
  return request({
    url: '/v3/account',
    method: 'GET',
    baseURL: _config.default.binance.url,
    params
  })
} const _default = { getBalances }; exports.default = _default
