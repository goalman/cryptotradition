const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _httpClient = _interopRequireDefault(require('../httpClient'))
const _config = _interopRequireDefault(require('../config'))


function getCandlestickData(params) {
  _httpClient.default.get('/v1/klines', {
    baseURL: _config.default.binance.url,
    params
  })
} const _default = getCandlestickData; exports.default = _default
