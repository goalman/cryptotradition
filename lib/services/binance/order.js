const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0

const _httpClient = _interopRequireDefault(require('../httpClient'))
const _config = _interopRequireDefault(require('../config'))


function putOrder(params) {
  _httpClient.default.post('/v3/order/test', {
    baseURL: _config.default.binance.url,
    headers: {
      'X-MBX-APIKEY': _config.default.binance.apiKey,
      'X-MBX-SECRET': _config.default.binance.secretKey
    },

    params
  })
}


function getOrder(params) {
  _httpClient.default.get('/v3/order', {
    baseURL: _config.default.binance.url,
    headers: {
      'X-MBX-APIKEY': _config.default.binance.apiKey,
      'X-MBX-SECRET': _config.default.binance.secretKey
    },

    params
  })
}


function deleteOrder(params) {
  _httpClient.default.delete('/v3/order', {
    baseURL: _config.default.binance.url,
    headers: {
      'X-MBX-APIKEY': _config.default.binance.apiKey,
      'X-MBX-SECRET': _config.default.binance.secretKey
    },

    params
  })
}


function getOpenOrders(params) {
  _httpClient.default.get('/v3/order', {
    baseURL: _config.default.binance.url,
    headers: {
      'X-MBX-APIKEY': _config.default.binance.apiKey,
      'X-MBX-SECRET': _config.default.binance.secretKey
    },

    params
  })
}


function getAccountTradeList(params) {
  _httpClient.default.get('/v3/myTrades', {
    baseURL: _config.default.binance.url,
    headers: {
      'X-MBX-APIKEY': _config.default.binance.apiKey,
      'X-MBX-SECRET': _config.default.binance.secretKey
    },

    params
  })
}


function getAveragePrice(params) {
  _httpClient.default.delete('/v3/avgPrice', {
    baseURL: _config.default.binance.url,
    params
  })
}


function getLatestPrice(params) {
  _httpClient.default.delete('/v1/ticker/price', {
    baseURL: _config.default.binance.url,
    params
  })
}


function get24hrPriceStatistics(params) {
  _httpClient.default.delete('/v3/ticker/24hr', {
    baseURL: _config.default.binance.url,
    params
  })
} const _default = (


  putOrder,
  getOrder,
  deleteOrder,
  getOpenOrders,
  getAccountTradeList,
  getAveragePrice,
  getLatestPrice,
  get24hrPriceStatistics); exports.default = _default
