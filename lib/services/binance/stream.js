const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _httpClient = _interopRequireDefault(require('../httpClient'))
const _config = _interopRequireDefault(require('../config'))

function getUserDataStream() {
  _httpClient.default.post('/userDataStream', {
    exchange: 'binance',
    apiVersion: 'v1',
    security: {
      apiKey: true,
      secretKey: true
    }
  })
}


function keepStreamAlive(params) {
  _httpClient.default.put('/v1/userDataStream', {
    baseURL: _config.default.binance.url,
    headers: {
      'X-MBX-APIKEY': _config.default.binance.apiKey,
      'X-MBX-SECRET': _config.default.binance.secretKey
    },

    params
  })
}


function closeStream(params) {
  _httpClient.default.delete('/v1/userDataStream', {
    baseURL: _config.default.binance.url,
    headers: {
      'X-MBX-APIKEY': _config.default.binance.apiKey,
      'X-MBX-SECRET': _config.default.binance.secretKey
    },

    params
  })
} const _default = (


  getUserDataStream,
  keepStreamAlive,
  closeStream); exports.default = _default
