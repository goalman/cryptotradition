const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _httpClient = _interopRequireDefault(require('../httpClient'))
const _config = _interopRequireDefault(require('../config'))

const

  { request } = _httpClient.default


function getPrices(params) {
  return request({
    url: '/data/price',
    method: 'GET',
    baseURL: _config.default.cryptoCompare.url,
    params
  })
} const _default = getPrices; exports.default = _default
