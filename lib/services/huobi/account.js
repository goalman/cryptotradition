const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _httpClient = _interopRequireDefault(require('../httpClient'))
const _config = _interopRequireDefault(require('../config'))

const

  { request } = _httpClient.default


function getAccount(data) {
  return request({
    url: '/v1/account/accounts',
    method: 'GET',
    baseURL: _config.default.huobi.url
  })
} const _default = { getAccount }; exports.default = _default
