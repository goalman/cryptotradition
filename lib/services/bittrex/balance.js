const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _httpClient = _interopRequireDefault(require('../httpClient'))
const _config = _interopRequireDefault(require('../config'))

const

  { request } = _httpClient.default


function getBalances() {
  return request({
    url: '/account/getbalances',
    method: 'GET',
    baseURL: _config.default.bittrex.url
  })
}


function getBalance(params) {
  return request({
    url: '/account/getbalance',
    method: 'GET',
    baseURL: _config.default.bittrex.url,
    params
  })
} const _default = {
  getBalances,
  getBalance
}; exports.default = _default
