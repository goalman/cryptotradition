const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _httpClient = _interopRequireDefault(require('../httpClient'))
const _config = _interopRequireDefault(require('../config'))

const

  { request } = _httpClient.default


function getBalances(data) {
  return request({
    url: '/v1/balances',
    method: 'POST',
    baseURL: _config.default.bitfinex.url,
    data
  })
} const _default = { getBalances }; exports.default = _default
