const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _httpClient = _interopRequireDefault(require('../httpClient'))
const _config = _interopRequireDefault(require('../config'))

const

  { request } = _httpClient.default


function getLatestExchangeRates(params) {
  return request({
    url: '/latest',
    method: 'GET',
    baseURL: _config.default.coinMarketCap.url,
    params
  })
} const _default = getCurrencyLatestData; exports.default = _default
