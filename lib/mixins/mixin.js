Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0
function setStateAsync(object, state) {
  return new Promise(((resolve) => {
    object.setState(state, resolve)
  }))
} const _default = setStateAsync; exports.default = _default
