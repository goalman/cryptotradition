const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _reactNavigationMaterialBottomTabs = require('react-navigation-material-bottom-tabs')
const _Ionicons = _interopRequireDefault(require('@expo/vector-icons/Ionicons'))
const _react = _interopRequireDefault(require('react'))
const _reactNative = require('react-native')
const _HomeScreen = _interopRequireDefault(require('./screens/HomeScreen'))
const _TradeScreen = _interopRequireDefault(require('./screens/TradeScreen'))
const _BalancesScreen = _interopRequireDefault(require('./screens/BalancesScreen'))
const _SettingsScreen = _interopRequireDefault(require('./screens/SettingsScreen'))

const _jsxFileName = 'C:\\Users\\tiboy\\IdeaProjects\\cryptotradition\\src\\containers\\Navigator.js'

const TabNavigator = (0, _reactNavigationMaterialBottomTabs.createMaterialBottomTabNavigator)(
  {
    Home: {
      screen: _HomeScreen.default,
      navigationOptions: {
        tabBarLabel: 'Domů',
        tabBarIcon: function tabBarIcon(_ref) {
          const { tintColor } = _ref; return (
            _react.default.createElement(_reactNative.View, { __source: { fileName: _jsxFileName, lineNumber: 17 } },
              _react.default.createElement(_Ionicons.default, {
                style: [{ color: tintColor }], size: 25, name: 'ios-home', __source: { fileName: _jsxFileName, lineNumber: 18 }
              })))
        },


        activeColor: '#f0edf6',
        inactiveColor: '#226557',
        barStyle: { backgroundColor: '#3BAD87' }
      }
    },


    Trade: {
      screen: _TradeScreen.default,
      navigationOptions: {
        tabBarLabel: 'Obchod',
        tabBarIcon: function tabBarIcon(_ref2) {
          const { tintColor } = _ref2; return (
            _react.default.createElement(_reactNative.View, { __source: { fileName: _jsxFileName, lineNumber: 31 } },
              _react.default.createElement(_Ionicons.default, {
                style: [{ color: tintColor }], size: 25, name: 'ios-person', __source: { fileName: _jsxFileName, lineNumber: 32 }
              })))
        },


        activeColor: '#f60c0d',
        inactiveColor: '#f65a22',
        barStyle: { backgroundColor: '#f69b31' }
      }
    },


    Balances: {
      screen: _BalancesScreen.default,
      navigationOptions: {
        tabBarLabel: 'Prostředky',
        tabBarIcon: function tabBarIcon(_ref3) {
          const { tintColor } = _ref3; return (
            _react.default.createElement(_reactNative.View, { __source: { fileName: _jsxFileName, lineNumber: 45 } },
              _react.default.createElement(_Ionicons.default, {
                style: [{ color: tintColor }], size: 25, name: 'ios-images', __source: { fileName: _jsxFileName, lineNumber: 46 }
              })))
        },


        activeColor: '#615af6',
        inactiveColor: '#46f6d7',
        barStyle: { backgroundColor: '#67baf6' }
      }
    },


    Settings: {
      screen: _SettingsScreen.default,
      navigationOptions: {
        tabBarLabel: 'Nastavení',
        tabBarIcon: function tabBarIcon(_ref4) {
          const { tintColor } = _ref4; return (
            _react.default.createElement(_reactNative.View, { __source: { fileName: _jsxFileName, lineNumber: 59 } },
              _react.default.createElement(_Ionicons.default, {
                style: [{ color: tintColor }], size: 25, name: 'ios-cart', __source: { fileName: _jsxFileName, lineNumber: 60 }
              })))
        },


        activeColor: '#2a2a2a',
        inactiveColor: '#727272',
        barStyle: { backgroundColor: '#535353' }
      }
    }
  },


  { initialRouteName: 'Home' }
); const _default = TabNavigator; exports.default = _default
