const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _extends2 = _interopRequireDefault(require('@babel/runtime/helpers/extends')); const _regenerator = _interopRequireDefault(require('@babel/runtime/regenerator')); const _asyncToGenerator2 = _interopRequireDefault(require('@babel/runtime/helpers/asyncToGenerator')); const _classCallCheck2 = _interopRequireDefault(require('@babel/runtime/helpers/classCallCheck')); const _createClass2 = _interopRequireDefault(require('@babel/runtime/helpers/createClass')); const _possibleConstructorReturn2 = _interopRequireDefault(require('@babel/runtime/helpers/possibleConstructorReturn')); const _getPrototypeOf2 = _interopRequireDefault(require('@babel/runtime/helpers/getPrototypeOf')); const _inherits2 = _interopRequireDefault(require('@babel/runtime/helpers/inherits'))
const _reactNative = require('react-native')


const _react = _interopRequireDefault(require('react'))
const _index = _interopRequireDefault(require('../../themes/index'))
const _balanceScreen = _interopRequireDefault(require('../../themes/balanceScreen'))
const _balance = _interopRequireDefault(require('../../services/binance/balance'))
const _balance2 = _interopRequireDefault(require('../../services/bittrex/balance'))
const _balance3 = _interopRequireDefault(require('../../services/bitfinex/balance'))
const _account = _interopRequireDefault(require('../../services/huobi/account'))
const _balance4 = _interopRequireDefault(require('../../services/huobi/balance'))
const _prices = _interopRequireDefault(require('../../services/cryptoCompare/prices'))
const _config = _interopRequireDefault(require('../../services/config'))
const _TopStatusBar = _interopRequireDefault(require('../../components/TopStatusBar'))
const _allBalances = require('../../redux/allBalances')
const _reactRedux = require('react-redux')
const _redux = require('redux')

const _jsxFileName = 'C:\\Users\\tiboy\\IdeaProjects\\cryptotradition\\src\\containers\\screens\\BalancesScreen.js'; const


  BalancesScreen = (function (_React$Component) {
    (0, _inherits2.default)(BalancesScreen, _React$Component)


    function BalancesScreen(props) {
      let _this; (0, _classCallCheck2.default)(this, BalancesScreen)
      _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(BalancesScreen).call(this, props)); _this.totalBalances = _config.default.cryptoCompare.currencies; _this.exchangeBalances = {
        binance: _config.default.cryptoCompare.currencies, bittrex: _config.default.cryptoCompare.currencies, bitfinex: _config.default.cryptoCompare.currencies, huobi: _config.default.cryptoCompare.currencies
      }; _this


        .setZeroBalances = (0, _asyncToGenerator2.default)(_regenerator.default.mark(function _callee() { return _regenerator.default.wrap((_context) => { while (1) { switch (_context.prev = _context.next) { case 0: case 'end': return _context.stop() } } }, _callee) })); _this


        .loadBalances = (0, _asyncToGenerator2.default)(_regenerator.default.mark(function _callee2() {
          return _regenerator.default.wrap((_context2) => {
            while (1) {
              switch (_context2.prev = _context2.next) {
              case 0: _context2.next = 2; return (
                _this.props.onAllBalancesRequest()); case 2: _context2.next = 4; return (


                _this.getApiBalances()); case 4: _context2.next = 6; return (
                _this.sumAllBalances()); case 6: _context2.next = 8; return (
                _this.getCzkRates()); case 8:
                _this.setState(() => ({ balances: _this.totalBalances }), () => {}); case 9: case 'end': return _context2.stop()
              }
            }
          }, _callee2)
        })); _this


        .getApiBalances = function () {
          return Promise.all([
            _this.getBinanceBalances(),
            _this.getBittrexBalances(),
            _this.getBitfinexBalances(),
            _this.getHuobiBalances()])
        }; _this

        .getBinanceBalances = function () {
          return _balance.default.getBalances().then((response) => {
            _this.addBinanceBalances(response.data.balances)
          }).catch((error) => {
            console.log(`got binance error${error}`)
          })
        }; _this

        .getBittrexBalances = function () {
          const promises = []
          _config.default.bittrex.currencies.forEach((currency) => {
            const params = new global.URLSearchParams()
            params.append('currency', currency)
            promises.push(_balance2.default.getBalance(params).then((response) => {
              _this.addBittrexBalance(response.data)
            }).catch((error) => {
              console.log(`got bittrex error${error}`)
            }))
          })
          return Promise.all(promises)
        }; _this

        .getBitfinexBalances = function () {
          return _balance3.default.getBalances().then(function () {
            const _ref3 = (0, _asyncToGenerator2.default)(_regenerator.default.mark(function _callee3(response) {
              return _regenerator.default.wrap((_context3) => {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                  case 0: _context3.next = 2; return (
                    _this.addBitfinexBalances(response.data)); case 2: case 'end': return _context3.stop()
                  }
                }
              }, _callee3)
            })); return function (_x) { return _ref3.apply(this, arguments) }
          }()).catch(
            (error) => {
              console.log(`got bitfinex error${error}`)
            }
          )
        }; _this

        .getHuobiBalances = function () {
          return _account.default.getAccount().then((response) => {
            const accountId = response.data.data.find((account) => account.type === 'spot').id
            return _balance4.default.getBalances(accountId).then(function () {
              const _ref4 = (0, _asyncToGenerator2.default)(_regenerator.default.mark(function _callee4(response) {
                return _regenerator.default.wrap((_context4) => {
                  while (1) {
                    switch (_context4.prev = _context4.next) {
                    case 0: _context4.next = 2; return (
                      _this.addHuobiBalances(response.data.data.list)); case 2: case 'end': return _context4.stop()
                    }
                  }
                }, _callee4)
              })); return function (_x2) { return _ref4.apply(this, arguments) }
            }()).catch(
              (error) => {
                console.log(`got huobi balance error${error}`)
              }
            )
          }).catch((error) => {
            console.log(`got huobi account error${error}`)
          })
        }; _this

        .addBinanceBalances = function (balances) {
          const filteredCurrencies = balances.filter((balance) => _config.default.binance.currencies.some((currency) => currency === balance.asset))
          filteredCurrencies.forEach((currency) => {
            if (currency.asset === 'BCC')currency.asset = 'BCH'
            _this.exchangeBalances.binance.forEach((obj, index) => {
              if (obj.symbol === currency.asset) {
                obj.balance = Number(currency.free) + Number(currency.locked)
                obj.available = Number(currency.free)
              }
            })
          })
        }; _this

        .addBittrexBalance = function (balance) {
          _this.exchangeBalances.bittrex.forEach((obj) => {
            if (obj.symbol === balance.result.Currency) {
              obj.balance = balance.result.Balance ? Number(balance.result.Balance) : 0
              obj.available = balance.result.Available ? Number(balance.result.Available) : 0
            }
          })
        }; _this

        .addBitfinexBalances = function (balances) {
          const filteredCurrencies = balances.filter((balance) => _config.default.bitfinex.currencies.some((currency) => currency === balance.currency.toUpperCase()))
          filteredCurrencies.forEach((currency) => {
            _this.exchangeBalances.bitfinex.forEach((obj) => {
              obj.balance = 0
              obj.available = 0
              if (obj.symbol === currency.currency.toUpperCase() && currency.type === 'exchange') {
                obj.balance = Number(currency.amount)
                obj.available = Number(currency.available)
              }
            })
          })
        }; _this

        .addHuobiBalances = (function () {
          const _ref5 = (0, _asyncToGenerator2.default)(_regenerator.default.mark(function _callee5(balances) {
            let filteredCurrencies; return _regenerator.default.wrap((_context5) => {
              while (1) {
                switch (_context5.prev = _context5.next) {
                case 0:

                  filteredCurrencies = balances.filter((balance) => _config.default.huobi.currencies.some((currency) => currency === balance.currency.toUpperCase()))
                  filteredCurrencies.forEach((currency) => {
                    _this.exchangeBalances.huobi.forEach((obj) => {
                      if (obj.symbol === currency.currency.toUpperCase()) {
                        obj.balance += Number(currency.balance)
                        obj.available = obj.type === 'trade' ? Number(currency.balance) : obj.available
                      }
                    })
                  }); case 2: case 'end': return _context5.stop()
                }
              }
            }, _callee5)
          })); return function (_x3) { return _ref5.apply(this, arguments) }
        }()); _this


        .getCzkRates = (0, _asyncToGenerator2.default)(_regenerator.default.mark(function _callee6() {
          let getTsymsParamValue; let params; return _regenerator.default.wrap((_context6) => {
            while (1) {
              switch (_context6.prev = _context6.next) {
              case 0:
                getTsymsParamValue = function getTsymsParamValue() {
                  let string = ''
                  _this.state.balances.forEach((currency) => {
                    string += `${currency.symbol},`
                  })
                  return string
                }
                params = new global.URLSearchParams()
                params.append('fsym', 'CZK')
                params.append('tsyms', getTsymsParamValue()); _context6.next = 6; return (
                  (0, _prices.default)(params).then((response) => {
                    _this.countCzkPrices(response.data)
                  }).catch((error) => {
                    console.log(`got cryptocompare error: ${error}`)
                  })); case 6: case 'end': return _context6.stop()
              }
            }
          }, _callee6)
        })); _this


        .sumAllBalances = function () {
          _this.totalBalances.forEach((totalBalance) => {
            Object.values(_this.exchangeBalances).forEach((exchangeBalances) => {
              for (const exchangeBalance in exchangeBalances) {
                (0, _extends2.default)(totalBalance, { balance: totalBalance.balance + (totalBalance.symbol === exchangeBalance.symbol ? exchangeBalance.balance : 0) })
              }
            })
          })
        }; _this

        .countCzkPrices = function (rates) {
          _this.totalBalances.forEach((obj) => {
            (0, _extends2.default)(obj, { czkBalance: obj.balance / rates[obj.symbol] })
          })
        }; _this

        .renderItem = function (_ref7) {
          const { item } = _ref7; return (
            _react.default.createElement(_reactNative.Text, { style: _index.default.item, __source: { fileName: _jsxFileName, lineNumber: 222 } },
              _react.default.createElement(_reactNative.Text, { __source: { fileName: _jsxFileName, lineNumber: 223 } }, item.name, ' '),
              _react.default.createElement(_reactNative.Text, { __source: { fileName: _jsxFileName, lineNumber: 224 } }, item.balance, ' '),
              _react.default.createElement(_reactNative.Text, { __source: { fileName: _jsxFileName, lineNumber: 225 } }, item.czkBalance, ' ')))
        }; _this.state = { loading: false, balances: _config.default.cryptoCompare.currencies }; return _this
    }(0, _createClass2.default)(BalancesScreen, [{ key: 'componentDidMount', value: (function () { const _componentDidMount = (0, _asyncToGenerator2.default)(_regenerator.default.mark(function _callee7() { return _regenerator.default.wrap(function _callee7$(_context7) { while (1) { switch (_context7.prev = _context7.next) { case 0: this.loadBalances(); case 1: case 'end': return _context7.stop() } } }, _callee7, this) })); function componentDidMount() { return _componentDidMount.apply(this, arguments) } return componentDidMount }()) }, {
      key: 'render',
      value: function render() {
        return (
          _react.default.createElement(_reactNative.SafeAreaView, { style: _index.default.container, __source: { fileName: _jsxFileName, lineNumber: 231 } },
            _react.default.createElement(_TopStatusBar.default, { backgroundColor: '#772ea2', barStyle: 'light-content', __source: { fileName: _jsxFileName, lineNumber: 232 } }),
            _react.default.createElement(_reactNative.View, { __source: { fileName: _jsxFileName, lineNumber: 233 } },
              _react.default.createElement(_reactNative.Text, { style: _balanceScreen.default.header, __source: { fileName: _jsxFileName, lineNumber: 234 } }, 'Prost\u0159edky'),
              _react.default.createElement(_reactNative.FlatList, {
                data: this.state.balances,
                renderItem: this.renderItem,
                keyExtractor: function keyExtractor(item) { return item.symbol },
                refreshControl:
_react.default.createElement(_reactNative.RefreshControl, {
  refreshing: this.state.loading,
  onRefresh: this.loadBalances,
  __source: { fileName: _jsxFileName, lineNumber: 240 }
}),
                __source: { fileName: _jsxFileName, lineNumber: 235 }
              }))))
      }
    }]); return BalancesScreen
  }(_react.default.Component))


const mapStateToProps = function mapStateToProps(state) {
  return { allBalances: state.allBalancesReducer.allBalances }
}

const mapDispatchToProps = function mapDispatchToProps(dispatch) {
  return (0, _redux.bindActionCreators)({ onAllBalancesRequest: _allBalances.onAllBalancesRequest }, dispatch)
}; const _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(BalancesScreen); exports.default = _default
