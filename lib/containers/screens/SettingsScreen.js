const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _classCallCheck2 = _interopRequireDefault(require('@babel/runtime/helpers/classCallCheck')); const _createClass2 = _interopRequireDefault(require('@babel/runtime/helpers/createClass')); const _possibleConstructorReturn2 = _interopRequireDefault(require('@babel/runtime/helpers/possibleConstructorReturn')); const _getPrototypeOf2 = _interopRequireDefault(require('@babel/runtime/helpers/getPrototypeOf')); const _inherits2 = _interopRequireDefault(require('@babel/runtime/helpers/inherits')); const _reactNative = require('react-native')
const _react = _interopRequireDefault(require('react'))
const _index = _interopRequireDefault(require('../../themes/index'))
const _TopStatusBar = _interopRequireDefault(require('../../components/TopStatusBar'))

const _jsxFileName = 'C:\\Users\\tiboy\\IdeaProjects\\cryptotradition\\src\\containers\\screens\\SettingsScreen.js'; const

  SettingsScreen = (function (_React$Component) {
    (0, _inherits2.default)(SettingsScreen, _React$Component); function SettingsScreen() { (0, _classCallCheck2.default)(this, SettingsScreen); return (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(SettingsScreen).apply(this, arguments)) }(0, _createClass2.default)(SettingsScreen, [{
      key: 'render',
      value: function render() {
        return (
          _react.default.createElement(_reactNative.SafeAreaView, { style: _index.default.container, __source: { fileName: _jsxFileName, lineNumber: 9 } },
            _react.default.createElement(_TopStatusBar.default, { backgroundColor: '#772ea2', barStyle: 'light-content', __source: { fileName: _jsxFileName, lineNumber: 10 } }),
            _react.default.createElement(_reactNative.View, { __source: { fileName: _jsxFileName, lineNumber: 11 } },
              _react.default.createElement(_reactNative.Text, { __source: { fileName: _jsxFileName, lineNumber: 12 } }, 'Settings Screen'))))
      }
    }]); return SettingsScreen
  }(_react.default.Component)); exports.default = SettingsScreen
