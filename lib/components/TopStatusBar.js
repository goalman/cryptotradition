const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _extends2 = _interopRequireDefault(require('@babel/runtime/helpers/extends')); const _objectWithoutProperties2 = _interopRequireDefault(require('@babel/runtime/helpers/objectWithoutProperties')); const _react = _interopRequireDefault(require('react'))
const _reactNative = require('react-native')
const _statusBar = _interopRequireDefault(require('../themes/statusBar'))

const _jsxFileName = 'C:\\Users\\tiboy\\IdeaProjects\\cryptotradition\\src\\components\\TopStatusBar.js'

const TopStatusBar = function TopStatusBar(_ref) {
  const { backgroundColor } = _ref; const
    props = (0, _objectWithoutProperties2.default)(_ref, ['backgroundColor']); return (
    _react.default.createElement(_reactNative.View, { style: [_statusBar.default.statusBar, { backgroundColor }], __source: { fileName: _jsxFileName, lineNumber: 6 } },
      _react.default.createElement(_reactNative.StatusBar, (0, _extends2.default)({ translucent: true, backgroundColor }, props, { __source: { fileName: _jsxFileName, lineNumber: 7 } }))))
}; const _default = TopStatusBar; exports.default = _default
