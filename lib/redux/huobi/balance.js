const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.setBinanceBalancesEpic = exports.getBinanceBalancesEpic = exports.huobiBalancesReducer = exports.onHuobiBalancesError = exports.onHuobiBalancesSuccess = exports.onHuobiBalancesRequest = exports.initialState = void 0; const _defineProperty2 = _interopRequireDefault(require('@babel/runtime/helpers/defineProperty')); const _rxjs = require('rxjs')
const _operators = require('rxjs/operators')
const _balance = _interopRequireDefault(require('../../services/binance/balance'))
const _balance2 = _interopRequireDefault(require('../../services/bittrex/balance'))
const _balance3 = _interopRequireDefault(require('../../services/bitfinex/balance'))
const _account = _interopRequireDefault(require('../../services/huobi/account'))
const _balance4 = _interopRequireDefault(require('../../services/huobi/balance'))


const _api = require('../services/api')
const _reduxObservable = require('redux-observable')
const _config = _interopRequireDefault(require('../../services/config'))
const _balance5 = require('../bitfinex/balance')

function ownKeys(object, enumerableOnly) { const keys = Object.keys(object); if (Object.getOwnPropertySymbols) { let symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly)symbols = symbols.filter((sym) => Object.getOwnPropertyDescriptor(object, sym).enumerable); keys.push.apply(keys, symbols) } return keys } function _objectSpread(target) { for (let i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach((key) => { (0, _defineProperty2.default)(target, key, source[key]) }) } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) } else { ownKeys(source).forEach((key) => { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)) }) } } return target }

const initialState = {
  loading: false,
  huobiBalances: [],
  error: null
}; exports.initialState = initialState

const onHuobiBalancesRequest = function onHuobiBalancesRequest() { return { type: 'ON_HUOBI_BALANCE_REQUEST' } }; exports.onHuobiBalancesRequest = onHuobiBalancesRequest

const onHuobiBalancesSuccess = function onHuobiBalancesSuccess() { return { type: 'ON_HUOBI_BALANCE_SUCCESS' } }; exports.onHuobiBalancesSuccess = onHuobiBalancesSuccess

const onHuobiBalancesError = function onHuobiBalancesError() { return { type: 'ON_HUOBI_BALANCE_ERROR' } }; exports.onHuobiBalancesError = onHuobiBalancesError


const huobiBalancesReducer = function huobiBalancesReducer() {
  const state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState; const action = arguments.length > 1 ? arguments[1] : undefined
  switch (action.type) {
  case 'ON_HUOBI_BALANCES_REQUEST':
    return _objectSpread({},
      state, { loading: true })

  case 'ON_HUOBI_BALANCES_SUCCESS':
    return _objectSpread({},
      state, {
        loading: false,
        items: action.movies.results
      })

  case 'ON_HUOBI_BALANCES_ERROR':
    return _objectSpread({},
      state, {
        loading: false,
        error: action.error
      })

  default:
    return state
  }
}; exports.huobiBalancesReducer = huobiBalancesReducer

const getBinanceBalancesEpic = function getBinanceBalancesEpic(action$) {
  return (
    action$.pipe(
      (0, _reduxObservable.ofType)(T.ON_HUOBI_BALANCES_REQUEST),
      (0, _rxjs.from)(_balance.default.getBalances()).pipe(
        map((response) => onHuobiBalancesSuccess(response.data.data.list)),
        (0, _operators.catchError)((e) => (0, _rxjs.of)(onHuobiBalancesError(e)))
      )
    ))
}; exports.getBinanceBalancesEpic = getBinanceBalancesEpic


const setBinanceBalancesEpic = function setBinanceBalancesEpic(action$) {
  return (
    action$.pipe(
      (0, _reduxObservable.ofType)(T.ON_HUOBI_BALANCES_SUCCESS),
      action$.huobiBalances.pipe(
        (0, _operators.filter)((balance) => (
          _config.default.huobi.currencies.some((currency) => currency === balance.currency.toUpperCase())))
      )
    ))
}; exports.setBinanceBalancesEpic = setBinanceBalancesEpic
