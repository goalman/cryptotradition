const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.binanceBalancesRequestEpic = exports.binanceBalancesReducer = exports.onBinanceBalanceError = exports.onBinanceBalanceSuccess = exports.onBinanceBalanceRequest = exports.initialState = void 0; const _defineProperty2 = _interopRequireDefault(require('@babel/runtime/helpers/defineProperty')); const _rxjs = require('rxjs')
const _operators = require('rxjs/operators')
const _balance = _interopRequireDefault(require('../../services/binance/balance'))
const _reduxObservable = require('redux-observable')
const _config = _interopRequireDefault(require('../../services/config'))

function ownKeys(object, enumerableOnly) { const keys = Object.keys(object); if (Object.getOwnPropertySymbols) { let symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly)symbols = symbols.filter((sym) => Object.getOwnPropertyDescriptor(object, sym).enumerable); keys.push.apply(keys, symbols) } return keys } function _objectSpread(target) { for (let i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach((key) => { (0, _defineProperty2.default)(target, key, source[key]) }) } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) } else { ownKeys(source).forEach((key) => { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)) }) } } return target }

const initialState = {
  loading: false,
  binanceBalances: _config.default.cryptoCompare.currencies,
  binanceError: null
}; exports.initialState = initialState


const onBinanceBalanceRequest = function onBinanceBalanceRequest() { return { type: 'ON_BINANCE_BALANCES_REQUEST' } }; exports.onBinanceBalanceRequest = onBinanceBalanceRequest

const onBinanceBalanceSuccess = function onBinanceBalanceSuccess(binanceBalances) {
  return {
    type: 'ON_BINANCE_BALANCES_SUCCESS',
    binanceBalances
  }
}; exports.onBinanceBalanceSuccess = onBinanceBalanceSuccess

const onBinanceBalanceError = function onBinanceBalanceError(errors) {
  return {
    type: 'ON_BINANCE_BALANCES_ERROR',
    errors
  }
}; exports.onBinanceBalanceError = onBinanceBalanceError


const binanceBalancesReducer = function binanceBalancesReducer() {
  const state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState; const action = arguments.length > 1 ? arguments[1] : undefined
  switch (action.type) {
  case 'ON_BINANCE_BALANCES_REQUEST':
    return _objectSpread({},
      state, { loading: true })

  case 'ON_BINANCE_BALANCES_SUCCESS':
    return _objectSpread({},
      state, {
        loading: false,
        binanceBalances: action.binanceBalances
      })

  case 'ON_BINANCE_BALANCES_ERROR':
    return _objectSpread({},
      state, {
        loading: false,
        error: action.error
      })

  default:
    return state
  }
}; exports.binanceBalancesReducer = binanceBalancesReducer

const binanceBalancesRequestEpic = function binanceBalancesRequestEpic(action$) {
  return action$.pipe(
    (0, _reduxObservable.ofType)('ON_BINANCE_BALANCES_REQUEST'),
    (0, _operators.switchMap)(() => (
      (0, _rxjs.from)(_balance.default.getBalances()).pipe(
        (0, _operators.flatMap)((response) => (0, _rxjs.from)([onBinanceBalanceSuccess(response.data.balances)]))
      )))
  )
}; exports.binanceBalancesRequestEpic = binanceBalancesRequestEpic


function setBinanceBalances(action$, state$) {
  action$.pipe(
    (0, _reduxObservable.ofType)(T.ON_BINANCE_BALANCES_SUCCESS),
    action$.binanceBalances.pipe(
      (0, _operators.filter)((balance) => _config.default.binance.currencies.some((currency) => currency === balance.asset))
    ),

    state$.binanceBalances.pipe(
      (0, _operators.map)((balance) => _config.default.cryptoCompare.currencies.forEach((currency) => {
        if (currency.asset === 'BCC')currency.asset = 'BCH'
        if (balance.symbol === currency.asset) {
          balance.balance = Number(currency.free) + Number(currency.locked)
          balance.available = Number(currency.free)
        }
      }))
    )
  )
}
