import type { State } from 'react-native/Libraries/Components/ScrollResponder'
import config from '../services/config'
import type ApisBalancesWithErrorsAndCzkRates from '../model/types/apisBalancesWithErrorsAndCzkRates'
import type AllBalancesWithErrors from '../model/types/allBalancesWithErrors'
import type AllBalancesWithErrorsAndTotalCzkBalance from '../model/types/allBalancesWithErrorsAndTotalCzkBalance'
import type BalancesWithCzkPrices from '../model/types/balancesWithCzkPrices'
import type ApiBalances from '../model/types/apiBalances'

/**
 * @param {InstanceType} componentInstance of component instance
 * @param {any} state to set
 * @returns {Promise<any>}
 */
export const setStateAsync = (componentInstance: InstanceType, state: State): Promise<any> => new Promise((resolve: any): Promise<any> => {
  componentInstance.setState(state, resolve)
})

/**
 * @param {object} allBalances
 * @returns {ApisBalancesWithErrorsAndCzkRates}
 */
export const sumBalances = (allBalances: ApisBalancesWithErrorsAndCzkRates): ApisBalancesWithErrorsAndCzkRates => {
  // array for sum balances
  const zeroBalances: BalancesWithCzkPrices = config.cryptoCompare.currencies.map((currency) => ({
    symbol: currency.symbol,
    name: currency.name,
    balance: 0,
    available: 0,
    czkBalance: 0,
  }))
  // object for balances and errors
  const allBalancesWithErrors: ApisBalancesWithErrorsAndCzkRates = {
    balances: zeroBalances,
    errors: [],
    czkRates: allBalances.czkRates
  }
  // add balances and errors into 'allBalancesWithErrors' object
  Object.entries(allBalances).forEach(([exchange: string, balances: Array<ApiBalances>]) => {
    // check if array doesn't contain czkRates or error
    if (exchange !== 'czkRates') {
      // prevent error
      if (balances.length === config.cryptoCompare.currencies.length) {
        balances.forEach((balance) => {
          allBalancesWithErrors.balances.find((zeroBalance) => zeroBalance.symbol === balance.symbol).balance += Number(balance.balance)
          allBalancesWithErrors.balances.find((zeroBalance) => zeroBalance.symbol === balance.symbol).available += Number(balance.available)
        })
      } else {
        allBalancesWithErrors.errors.push({ [exchange]: balances })
      }
    } else if (Object.keys(allBalances.czkRates).length !== config.cryptoCompare.currencies.length) {
      allBalancesWithErrors.errors.push({ [exchange]: balances })
    }
  })
  return allBalancesWithErrors
}

/**
 * @param {ApisBalancesWithErrorsAndCzkRates} allBalances to count czk prices
 * @returns {AllBalancesWithErrors} with czk prices
 */
export const countCzkPrices = (allBalances: ApisBalancesWithErrorsAndCzkRates): AllBalancesWithErrors => {
  allBalances.balances.forEach((balance) => {
    Object.assign(balance, { czkBalance: balance.balance / allBalances.czkRates[balance.symbol] })
  })
  // delete 'czkRates' property from 'allBalances'
  delete allBalances.czkRates

  return allBalances
}

/**
 * @param {AllBalancesWithErrors} allBalances
 * @returns {AllBalancesWithErrorsAndTotalCzkBalance}
 */
export const countTotalCzkBalance = (allBalances: AllBalancesWithErrors): AllBalancesWithErrorsAndTotalCzkBalance => {
  // set new property 'totalCzkBalance' to object 'allBalances'
  const AllBalancesWithTotalBalance = Object.assign(allBalances, { totalCzkBalance: 0 })
  // sum ap all czk balances
  AllBalancesWithTotalBalance.balances.forEach((balance) => {
    AllBalancesWithTotalBalance.totalCzkBalance += balance.czkBalance
  })
  return AllBalancesWithTotalBalance
}
