import config from '../services/config'

export const filterHuobiBalances = (balances) =>
  // binance returns all currencies on exchange
  balances.filter((balance) => config.huobi.currencies.some((currency) => currency === balance.currency.toUpperCase()))


export const mapHuobiBalances = (balances) => {
  const zeroBalances = config.huobi.currencies.map((currency) => ({
    symbol: currency,
    balance: 0,
    available: 0
  }))
  zeroBalances.forEach((obj) => {
    balances.forEach((balance) => {
      if (obj.symbol === balance.currency.toUpperCase()) {
        obj.balance += Number(balance.balance)
        obj.available = obj.type === 'trade' ? Number(balance.balance) : obj.available
      }
    })
  })
  return zeroBalances
}

// TODO sort by CZK
export const sortHuobiBalances = (balances) => balances.sort((a, b) => a.balance.toString().localeCompare(b.balance.toString()))
