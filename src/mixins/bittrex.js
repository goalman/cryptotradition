
export const mapBittrexBalances = (balances) => balances.map((balance) => ({
  symbol: balance.Currency,
  balance: balance.Balance ? Number(balance.Balance) : 0,
  available: balance.Available ? Number(balance.Available) : 0
}))

// TODO sort by CZK
export const sortBittrexBalances = (balances) => balances.sort((a, b) => a.balance.toString().localeCompare(b.balance.toString()))
