// @flow

/**
 * @param {number} number to round
 * @param {number} precision of rounding
 * @returns {number} rounded number
 */
export const roundNumber = (number: number, precision: number): number => {
  let editedPrecision = 1
  for (let i = 1; i <= precision; i++) {
    editedPrecision *= 10
  }
  return Math.round(number * editedPrecision + Number.EPSILON) / editedPrecision
}

/**
 * @param {string} string to cut
 * @param {number} maxChars of string
 * @returns {string} cut
 */
export const maxStringLength = (string: string, maxChars: number): string => string.substring(0, maxChars)

/**
 * @param {number} number to cut
 * @param {number} maxChars of string
 * @returns {string} cut
 */
export const numberFormatToShow = (number: number, maxChars: number): string => {
  let numberString: string = decimalPointToComma(number)
  let stringLength = maxChars
  const arrayOfLetterK = []
  while (numberString.length > maxChars) {
    numberString = numberString.substring(0, stringLength)
    if (numberString.includes(',')) {
      stringLength--
    } else {
      stringLength -= 3 // 3 chars for letter K which stands for three zeros
      arrayOfLetterK.push('K')
    }
  }
  if (numberString.endsWith(',')) {
    numberString.replace(',', '')
  }
  if (arrayOfLetterK.length) {
    arrayOfLetterK.forEach((letterK) => {
      numberString += letterK
    })
  }
  return numberString
}

/**
 * @param {number} number to edit
 * @returns {string} with comma
 */
export const decimalPointToComma = (number: number): string => {
  const numberString: string = number.toString()
  return numberString.replace('.', ',')
}
