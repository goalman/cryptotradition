import config from '../services/config'

// bitfinex returns only non zero balances and different exchange types
export const filterBitfinexBalances = (balances) => balances.filter((balance) => config.bitfinex.currencies.some(
  (currency) => currency === balance.currency.toUpperCase() && balance.type === 'exchange'
))

// bitfinex returns only non zero balances
export const mapBitfinexBalances = (balances) => config.bitfinex.currencies.map((currency) => {
  const foundBalance = balances.find((balance) => balance.currency.toUpperCase() === currency)
  return {
    symbol: currency,
    balance: foundBalance ? Number(foundBalance.amount) : 0,
    available: foundBalance ? Number(foundBalance.available) : 0
  }
})

// TODO sort by CZK
export const sortBitfinexBalances = (balances) => balances.sort((a, b) => a.balance.toString().localeCompare(b.balance.toString()))
