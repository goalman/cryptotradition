import config from '../services/config'

// binance returns all currencies on exchange
export const filterBinanceBalances = (balances) => balances.filter((balance) => config.binance.currencies.some((currency) => currency === balance.asset))

export const mapBinanceBalances = (balances) => balances.map((balance) => {
  if (balance.asset === 'BCC') balance.asset = 'BCH'
  return {
    symbol: balance.asset,
    balance: Number(balance.free) + Number(balance.locked),
    available: Number(balance.free)
  }
})

// TODO sort by CZK
export const sortBinanceBalances = (balances) => balances.sort((a, b) => a.balance.toString().localeCompare(b.balance.toString()))
