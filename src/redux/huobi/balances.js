import { from, Observable, of } from 'rxjs'
import {
  filter, switchMap, flatMap, catchError
} from 'rxjs/operators'
import { ofType } from 'redux-observable'
import binanceBalanceService from '../../services/binance/balance'
import bittrexBalanceService from '../../services/bittrex/balance'
import bitfinexBalanceService from '../../services/bitfinex/balance'
import huobiAccountService from '../../services/huobi/account'
import huobiBalanceService from '../../services/huobi/balance'

// api
import { getMovies } from '../services/api'
import config from '../../services/config'
import { onBitfinexBalanceError, onBitfinexBalanceSuccess } from '../bitfinex/balances'

export const initialState = {
  loading: false,
  huobiBalances: [],
  error: null,
}
export const onHuobiBalancesRequest = () => ({
  type: 'ON_HUOBI_BALANCE_REQUEST',
})
export const onHuobiBalancesSuccess = () => ({
  type: 'ON_HUOBI_BALANCE_SUCCESS',
})
export const onHuobiBalancesError = () => ({
  type: 'ON_HUOBI_BALANCE_ERROR',
})

export const huobiBalancesReducer = (state = initialState, action) => {
  switch (action.type) {
  case 'ON_HUOBI_BALANCES_REQUEST':
    return {
      ...state,
      loading: true,
    }
  case 'ON_HUOBI_BALANCES_SUCCESS':
    return {
      ...state,
      loading: false,
      items: action.movies.results,
    }
  case 'ON_HUOBI_BALANCES_ERROR':
    return {
      ...state,
      loading: false,
      error: action.error,
    }
  default:
    return state
  }
}

export const getBinanceBalancesEpic = (action$) => (
  action$.pipe(
    ofType(T.ON_HUOBI_BALANCES_REQUEST),
    from(binanceBalanceService.getBalances()).pipe( // getMovie is from api.js
      map((response) => onHuobiBalancesSuccess(response.data.data.list)),
      catchError((e) => of(onHuobiBalancesError(e)))
    )
  )
)

export const setBinanceBalancesEpic = (action$) => (
  action$.pipe(
    ofType(T.ON_HUOBI_BALANCES_SUCCESS),
    action$.huobiBalances.pipe(
      filter((balance) => config.huobi.currencies.some((currency) => currency === balance.currency.toUpperCase()))
    )
  )
)
