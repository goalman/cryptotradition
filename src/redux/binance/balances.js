import { from, of } from 'rxjs'
import {
  catchError, map, switchMap, flatMap
} from 'rxjs/operators'
import { ofType } from 'redux-observable'
import binanceBalanceService from '../../services/binance/balance'
import { filterBinanceBalances, mapBinanceBalances } from '../../mixins/binance'

export const initialState = {
  loading: false,
  binanceBalances: [],
  error: null,
}

export const onBinanceBalancesRequest = () => ({
  type: 'ON_BINANCE_BALANCES_REQUEST',
})
export const onBinanceBalancesSuccess = (binanceBalances) => ({
  type: 'ON_BINANCE_BALANCES_SUCCESS',
  binanceBalances
})
export const onBinanceBalancesError = (error) => ({
  type: 'ON_BINANCE_BALANCES_ERROR',
  error
})

export const binanceBalancesReducer = (state = initialState, action) => {
  switch (action.type) {
  case 'ON_BINANCE_BALANCES_REQUEST':
    return {
      ...state,
      loading: true,
    }
  case 'ON_BINANCE_BALANCES_SUCCESS':
    return {
      ...state,
      loading: false,
      binanceBalances: action.binanceBalances,
    }
  case 'ON_BINANCE_BALANCES_ERROR':
    return {
      ...state,
      loading: false,
      errors: [...this.errors, action.error]
    }
  default:
    return state
  }
}

export const binanceBalancesRequestEpic = (action$) => action$.pipe(
  ofType('ON_BINANCE_BALANCES_REQUEST'),
  switchMap(() => from(binanceBalanceService.getBalances()).pipe(
    map((response) => filterBinanceBalances(response.data.balances)),
    map((balances) => mapBinanceBalances(balances)),
    flatMap((response) => from([onBinanceBalancesSuccess(response)])),
    catchError((error) => of(onBinanceBalancesError(error))),

  ))
)
