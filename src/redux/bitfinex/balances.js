import { from, Observable, of } from 'rxjs'
import {
  filter, switchMap, flatMap, catchError
} from 'rxjs/operators'
import { ofType } from 'redux-observable'
import binanceBalanceService from '../../services/binance/balance'
import bittrexBalanceService from '../../services/bittrex/balance'
import bitfinexBalanceService from '../../services/bitfinex/balance'
import huobiAccountService from '../../services/huobi/account'
import huobiBalanceService from '../../services/huobi/balance'

// api
import { getMovies } from '../services/api'
import config from '../../services/config'
import { onBinanceBalanceError, onBinanceBalanceSuccess } from '../binance/balances'

export const initialState = {
  loading: false,
  bitfinexBalances: [],
  errors: [],
}
export const onBitfinexBalanceRequest = () => ({
  type: 'ON_BITFINEX_BALANCE_REQUEST',
})
export const onBitfinexBalanceSuccess = () => ({
  type: 'ON_BITFINEX_BALANCE_SUCCESS',
})
export const onBitfinexBalanceError = (error) => ({
  type: 'ON_BITFINEX_BALANCE_FAIL',
  error
})

export const bitfinexBalanceReducer = (state = initialState, action) => {
  switch (action.type) {
  case 'ON_BITFINEX_BALANCES_REQUEST':
    return {
      ...state,
      loading: true,
    }
  case 'ON_BITFINEX_BALANCES_SUCCESS':
    return {
      ...state,
      loading: false,
      items: action.bitfinexBalances,
    }
  case 'ON_BITFINEX_BALANCES_ERROR':
    return {
      ...state,
      loading: false,
      errors: action.error,
    }
  default:
    return state
  }
}

export const getBinanceBalancesEpic = (action$) => (
  action$.pipe(
    ofType(T.ON_BITFINEX_BALANCES_REQUEST),
    from(binanceBalanceService.getBalances()).pipe( // getMovie is from api.js
      map((response) => onBitfinexBalanceSuccess(response.data)),
      catchError((e) => of(onBitfinexBalanceError(e)))
    )
  )
)

export const setBinanceBalancesEpic = (action$) => (
  action$.pipe(
    ofType(T.ON_BITFINEX_BALANCES_SUCCESS),
    action$.binanceBalances.pipe(
      filter((balance) => config.bitfinex.currencies.some((currency) => currency === balance.currency.toUpperCase()))
    )
  )
)
