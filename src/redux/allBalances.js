import { from, of, forkJoin } from 'rxjs'
import {
  catchError, map, switchMap, flatMap
} from 'rxjs/operators'
import { ofType } from 'redux-observable'
import binanceBalanceService from '../services/binance/balance'
import bittrexBalanceService from '../services/bittrex/balance'
import bitfinexBalanceService from '../services/bitfinex/balance'
import huobiBalanceService from '../services/huobi/balance'
import cryptoCompareService from '../services/cryptoCompare/prices'
import { filterBinanceBalances, mapBinanceBalances } from '../mixins/binance'
import { filterBitfinexBalances, mapBitfinexBalances } from '../mixins/bitfinex'
import { filterHuobiBalances, mapHuobiBalances } from '../mixins/huobi'
import { mapBittrexBalances } from '../mixins/bittrex'
import { sumBalances, countCzkPrices, countTotalCzkBalance } from '../mixins/mixin'

export const initialState = {
  loading: false,
  allBalances: []
}

export const onAllBalancesRequest = () => ({
  type: 'ON_ALL_BALANCES_REQUEST',
})
export const onAllBalancesSuccess = (allBalances) => ({
  type: 'ON_ALL_BALANCES_SUCCESS',
  allBalances
})

export const allBalancesReducer = (state = initialState, action) => {
  switch (action.type) {
  case 'ON_ALL_BALANCES_REQUEST':
    return {
      ...state,
      loading: true,
    }
  case 'ON_ALL_BALANCES_SUCCESS':
    return {
      ...state,
      loading: false,
      allBalances: action.allBalances,
    }
  default:
    return state
  }
}

export const allBalancesRequestEpic = (action$) => action$.pipe(
  ofType('ON_ALL_BALANCES_REQUEST'),
  switchMap(() => forkJoin({
    binanceBalances: from(binanceBalanceService.getBalances()).pipe(
      map((response) => filterBinanceBalances(response.data.balances)),
      map((balances) => mapBinanceBalances(balances)),
      catchError((error) => of(error))
    ),
    bittrexBalances: from(bittrexBalanceService.getSupportedBalances()).pipe(
      map((balances) => mapBittrexBalances(balances)),
      catchError((error) => of(error))
    ),
    bitfinexBalances: from(bitfinexBalanceService.getBalances()).pipe(
      map((response) => filterBitfinexBalances(response.data)),
      map((balances) => mapBitfinexBalances(balances)),
      catchError((error) => of(error))
    ),
    huobiBalances: from(huobiBalanceService.getHuobiAccountBalances()).pipe(
      map((response) => filterHuobiBalances(response.data.data.list)),
      map((balances) => mapHuobiBalances(balances)),
      catchError((error) => of(error))
    ),
    czkRates: from(cryptoCompareService.getSupportedCurrenciesCzkRates()).pipe(
      map((response) => response.data),
      catchError((error) => of(error))
    )
  })),
  map((allBalances) => sumBalances(allBalances)),
  map((allBalances) => countCzkPrices(allBalances)),
  map((allBalances) => countTotalCzkBalance(allBalances)),
  flatMap((allBalances) => from([onAllBalancesSuccess(allBalances)]))
)
