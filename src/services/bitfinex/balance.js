import httpClient from '../httpClient'
import config from '../config'

const { request } = httpClient

// https://docs.bitfinex.com/v1/reference#rest-auth-wallet-balances
function getBalances(data) {
  return request({
    url: '/v1/balances',
    method: 'POST',
    baseURL: config.bitfinex.url,
    data,
  })
}

export default {
  getBalances
}
