import httpClient from '../httpClient'
import config from '../config'

const { request } = httpClient

// https://min-api.cryptocompare.com/documentation
function getPrices(params) {
  return request({
    url: '/data/price',
    method: 'GET',
    baseURL: config.cryptoCompare.url,
    params
  })
}

function getSupportedCurrenciesCzkRates() {
  const getTsymsParamValue = () => {
    let string = ''
    config.cryptoCompare.currencies.forEach((currency) => {
      string += `${currency.symbol},`
    })
    return string
  }
  const params = new global.URLSearchParams()
  params.append('fsym', 'CZK')
  params.append('tsyms', getTsymsParamValue())
  return getPrices(params)
}

export default {
  getPrices,
  getSupportedCurrenciesCzkRates
}
