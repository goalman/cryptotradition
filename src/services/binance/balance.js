import httpClient from '../httpClient'
import config from '../config'

const { request } = httpClient

// Nam        Type  Mandatory
// recvWindow LONG  NO
// timestamp  LONG  YES
function getBalances(params) {
  return request({
    url: '/v3/account',
    method: 'GET',
    baseURL: config.binance.url,
    params,
  })
}

export default {
  getBalances
}
