import request from '../httpClient'
import config from '../config'

// Name       Type    Mandatory  Description
// symbol     STRING  YES
// interval   ENUM    YES
// startTime  LONG    NO
// endTime    LONG    NO
// limit      INT     NO         Default 500; max 1000.
function getCandlestickData(params) {
  request.get('/v1/klines', {
    baseURL: config.binance.url,
    params
  })
}

export default (
  getCandlestickData
)
