import httpClient from '../httpClient'
import config from '../config'

const { request } = httpClient

// https://bittrex.github.io/api/v1-1
function getBalances() {
  return request({
    url: '/account/getbalances',
    method: 'GET',
    baseURL: config.bittrex.url
  })
}

// https://bittrex.github.io/api/v1-1
function getBalance(params) {
  return request({
    url: '/account/getbalance',
    method: 'GET',
    baseURL: config.bittrex.url,
    params
  })
}

function getSupportedBalances() {
  const promises = []
  config.bittrex.currencies.forEach((currency) => {
    const params = new global.URLSearchParams()
    params.append('currency', currency)
    promises.push(getBalance(params).then((response) => response.data.result))
  })
  return Promise.all(promises)
}

export default {
  getBalances,
  getBalance,
  getSupportedBalances
}
