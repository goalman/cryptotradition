import Axios from 'axios'
import Crypto from 'crypto-js'
import config from './config'

// Create an instance using the config defaults provided by the library
// At this point the timeout config value is `0` as is the default for the library
const client = Axios.create()

client.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
client.defaults.timeout = 2500

// Add a request interceptor
client.interceptors.request.use((request) => {
  if (!request.params) {
    request.params = new global.URLSearchParams()
  }
  if (request.baseURL === config.binance.url) {
    request.params.append('timestamp', Date.now().toString())
    request.params.append('signature', Crypto.HmacSHA256(request.params.toString(), config.binance.secretKey))
    request.headers = { 'X-MBX-APIKEY': config.binance.apiKey }
  }
  if (request.baseURL === config.bittrex.url) {
    request.params.append('apikey', config.bittrex.apiKey)
    request.params.append('nonce', Math.floor(Date.now() / 1000).toString())
    request.headers = {
      apisign: Crypto.HmacSHA512(
        `${request.baseURL}${request.url}?${request.params.toString()}`,
        config.bittrex.secretKey
      )
    }
  }
  if (request.baseURL === config.bitfinex.url) {
    request.data = JSON.stringify({
      ...request.data,
      request: request.url,
      nonce: (Date.now() * 1000).toString()
    })
    request.headers = {
      'Content-Type': 'application/json',
      'X-BFX-APIKEY': config.bitfinex.apiKey,
      'X-BFX-PAYLOAD': Crypto.enc.Base64.stringify(Crypto.enc.Utf8.parse(request.data)),
      'X-BFX-SIGNATURE': Crypto.HmacSHA384(Crypto.enc.Base64.stringify(Crypto.enc.Utf8.parse(request.data)),
        config.bitfinex.secretKey)
    }
  }
  if (request.baseURL === config.huobi.url) {
    request.params.append('AccessKeyId', config.huobi.apiKey)
    request.params.append('SignatureMethod', 'HmacSHA256')
    request.params.append('SignatureVersion', '2')
    request.params.append('Timestamp', new Date().toISOString().substring(0, 19))
    request.params.sort()
    const payload = `${request.method.toUpperCase()}\n${request.baseURL.replace('https://', '')}\n${request.url}\n${request.params.toString()}`
    request.params.append('Signature', Crypto.enc.Base64.stringify(Crypto.HmacSHA256(payload, config.huobi.secretKey)))
  }
  if (request.baseURL === config.cryptoCompare.url) {
    request.headers = { authorization: `Apikey${config.cryptoCompare.apiKey}` }
  }
  // console.log(request)
  return request
})

client.interceptors.response.use((response) =>
  // Do something with response data
  // console.log(response) // eslint-disable-line
  response,
(error) => {
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    console.log(error.response.data) // eslint-disable-line
    console.log(error.response.status) // eslint-disable-line
    console.log(error.response.headers) // eslint-disable-line
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    console.log(error.request) // eslint-disable-line
  } else {
    // Something happened in setting up the request that triggered an Error
    console.log('Error', error.message) // eslint-disable-line
  }
  console.log(error.config) // eslint-disable-line
  return Promise.reject(error)
})

const request = (options) => client.request(options)

export default {
  client,
  request
}
