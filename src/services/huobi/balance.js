import httpClient from '../httpClient'
import config from '../config'
import huobiAccountService from './account'

const { request } = httpClient

// huobi.readme.io/reference#get_v1-account-accounts
function getBalances(accountId) {
  return request({
    url: `/v1/account/accounts/${accountId}/balance`,
    method: 'GET',
    baseURL: config.huobi.url,
  })
}

// return balances for signed account
function getHuobiAccountBalances() {
  return huobiAccountService.getAccount().then((response) => {
    const accountId = response.data.data.find((account) => account.type === 'spot').id
    return getBalances(accountId)
  })
}

export default {
  getBalances,
  getHuobiAccountBalances
}
