import httpClient from '../httpClient'
import config from '../config'

const { request } = httpClient

// huobi.readme.io/reference#get_v1-account-accounts
function getAccount(data) {
  return request({
    url: '/v1/account/accounts',
    method: 'GET',
    baseURL: config.huobi.url,
  })
}

export default {
  getAccount
}
