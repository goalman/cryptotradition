export default {
  binance: {
    url: 'https://api.binance.com/api',
    requestTimeout: 1000,
    apiKey: 'rleG4Vhrf6UDWzkjXL3soe9XVWea9USYPYxjDbmvTSyPjx99vGKf0dH5G8PbNBVy',
    secretKey: 'y1i6cos8kyESdF6V5lK2KRXqgnVQpBHPVmddgfbjgogaFrXQadE8IvBqxWntWQpM',
    currencies: ['BTC', 'ETH', 'XRP', 'BCC', 'EOS', 'ADA', 'LTC', 'TRX', 'USDT', 'NEO']
  },

  bittrex: {
    url: 'https://api.bittrex.com/api/v1.1',
    requestTimeout: 1000,
    apiKey: '4da5b1a5207347c1a35703c0e41afd2d',
    secretKey: '7b4cc64676284c758a26e4d61bb043e3',
    currencies: ['BTC', 'ETH', 'XRP', 'BCH', 'EOS', 'ADA', 'LTC', 'TRX', 'USDT', 'NEO']
  },

  bitfinex: {
    url: 'https://api.bitfinex.com',
    apiKey: 'fjusjRrUlMtKwYbRUnCkML0dR4oo0ra77RWCQ2wMfLG',
    secretKey: '07iY0Z7IjL7d7WNB6dTaPQox332zodhZ6aTfLyxHKXz',
    currencies: ['BTC', 'ETH', 'XRP', 'BCH', 'EOS', 'ADA', 'LTC', 'TRX', 'USDT', 'NEO']
  },

  huobi: {
    url: 'https://api.huobi.pro',
    apiKey: 'd2e0da30-24bc9bd0-45285602-xa2b53ggfc',
    secretKey: '00fd4705-6509351a-2a72fe67-e4f0c',
    currencies: ['BTC', 'ETH', 'XRP', 'BCH', 'EOS', 'ADA', 'LTC', 'TRX', 'USDT', 'NEO']
  },

  // coinMarketCap: {
  //   url: 'https://pro-api.coinmarketcap.com',
  //   apiKey: '33cb1ee6-2b5c-40dc-a6fb-5fba6fc4c4b9',
  //   currencies: [
  //     {
  //       name: 'Bitcoin',
  //       symbol: 'BTC'
  //     },
  //     {
  //       name: 'Ethereum',
  //       symbol: 'ETH'
  //     },
  //     {
  //       name: 'Ripple',
  //       symbol: 'XRP'
  //     },
  //     {
  //       name: 'Bitcoin Cash',
  //       symbol: 'BCH'
  //     },
  //     {
  //       name: 'EOS',
  //       symbol: 'EOS'
  //     },
  //     {
  //       name: 'Cardano',
  //       symbol: 'ADA'
  //     },
  //     {
  //       name: 'Litecoin',
  //       symbol: 'LTC'
  //     },
  //     {
  //       name: 'Tron',
  //       symbol: 'TRX'
  //     },
  //     {
  //       name: 'Tether',
  //       symbol: 'USDT'
  //     },
  //     {
  //       name: 'NEO',
  //       symbol: 'NEO'
  //     }
  //   ]
  // },

  cryptoCompare: {
    url: 'https://min-api.cryptocompare.com',
    apiKey: 'c62131a0aa0dbff855e78d0fb4bee96cba65cfcf066ae9fef8f2135ca50bdec4',
    currencies: [
      {
        name: 'Bitcoin',
        symbol: 'BTC'
      },
      {
        name: 'Ethereum',
        symbol: 'ETH'
      },
      {
        name: 'Ripple',
        symbol: 'XRP'
      },
      {
        name: 'Bitcoin Cash',
        symbol: 'BCH'
      },
      {
        name: 'EOS',
        symbol: 'EOS'
      },
      {
        name: 'Cardano',
        symbol: 'ADA'
      },
      {
        name: 'Litecoin',
        symbol: 'LTC'
      },
      {
        name: 'Tron',
        symbol: 'TRX'
      },
      {
        name: 'Tether',
        symbol: 'USDT'
      },
      {
        name: 'NEO',
        symbol: 'NEO'
      }
    ]
  },

  euroCentralBank: {
    url: 'https://api.exchangeratesapi.io'
  }
}
