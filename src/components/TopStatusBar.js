import React from 'react'
import { View, StatusBar } from 'react-native'
import statusBarStyle from '../themes/statusBar'

const TopStatusBar = ({ backgroundColor, ...props }) => (
  <View style={[statusBarStyle.statusBar, { backgroundColor }]}>
    <StatusBar translucent backgroundColor={backgroundColor} {...props} />
  </View>
)

export default TopStatusBar
