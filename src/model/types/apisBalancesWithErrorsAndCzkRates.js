interface ApisBalancesWithErrorsAndCzkRates {
  balances: Array<Object>,
  czkRates: Array<Object>,
  errors: Array<any>
}

export default ApisBalancesWithErrorsAndCzkRates
