interface AllBalancesWithErrors {
  balances: Array<Object>,
  errors: Array<any>
}

export default AllBalancesWithErrors
