interface ApiBalances {
  symbol: string,
  balance: number,
  available: number,
}

export default ApiBalances
