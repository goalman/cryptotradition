interface BalancesWithCzkPrices {
  symbol: string,
  name: string,
  balance: number,
  available: number,
  czkBalance: number,
}

export default BalancesWithCzkPrices
