interface AllBalancesWithErrorsAndTotalCzkBalance {
  balances: Array<Object>,
  totalCzkBalance: number,
  errors: Array<any>
}

export default AllBalancesWithErrorsAndTotalCzkBalance
