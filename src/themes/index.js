import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    color: 'black'
  },
  item: {
    alignItems: 'flex-start',
    fontSize: 30,
    color: 'black'
  }
})
