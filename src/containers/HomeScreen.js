import {SafeAreaView, Text} from 'react-native'
import React from 'react'
import styles from '../themes/index'

export default class HomeScreen extends React.Component {
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <Text>Home Screen</Text>
            </SafeAreaView>
        );
    }
}