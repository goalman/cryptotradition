import {SafeAreaView, Text} from 'react-native'
import React from 'react'

export default class HomeScreen extends React.Component {
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <Text>Home Screen</Text>
            </SafeAreaView>
        );
    }
}
