import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'
import Icon from 'react-native-vector-icons/Ionicons'
import React from 'react'
import { View } from 'react-native'
import HomeScreen from './screens/HomeScreen'
import TradeScreen from './screens/TradeScreen'
import BalancesScreen from './screens/BalancesScreen'
import SettingsScreen from './screens/SettingsScreen'

const TabNavigator = createMaterialBottomTabNavigator(
  {
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        tabBarLabel: 'Domů',
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon style={[{ color: tintColor }]} size={25} name="ios-home" />
          </View>
        ),
        activeColor: '#f0edf6',
        inactiveColor: '#226557',
        barStyle: { backgroundColor: '#3BAD87' },
      }
    },
    Trade: {
      screen: TradeScreen,
      navigationOptions: {
        tabBarLabel: 'Obchod',
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon style={[{ color: tintColor }]} size={25} name="ios-person" />
          </View>
        ),
        activeColor: '#f60c0d',
        inactiveColor: '#f65a22',
        barStyle: { backgroundColor: '#f69b31' },
      }
    },
    Balances: {
      screen: BalancesScreen,
      navigationOptions: {
        tabBarLabel: 'Prostředky',
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon style={[{ color: tintColor }]} size={25} name="ios-images" />
          </View>
        ),
        activeColor: '#615af6',
        inactiveColor: '#46f6d7',
        barStyle: { backgroundColor: '#67baf6' },
      }
    },
    Settings: {
      screen: SettingsScreen,
      navigationOptions: {
        tabBarLabel: 'Nastavení',
        tabBarIcon: ({ tintColor }) => (
          <View>
            <Icon style={[{ color: tintColor }]} size={25} name="ios-cart" />
          </View>
        ),
        activeColor: '#2a2a2a',
        inactiveColor: '#727272',
        barStyle: { backgroundColor: '#535353' },
      }
    },
  },
  {
    initialRouteName: 'Home'
  },
)

export default TabNavigator
