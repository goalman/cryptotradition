// @flow
import {
  FlatList, Text, View, SafeAreaView, RefreshControl
} from 'react-native'
import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import styles from '../../themes/index'
import balancesStyle from '../../themes/balanceScreen'
import TopStatusBar from '../../components/TopStatusBar'
import { onAllBalancesRequest } from '../../redux/allBalances'
import { roundNumber, numberFormatToShow } from '../../mixins/filters'
import type AllBalancesWithErrorsAndTotalCzkBalance from '../../model/types/allBalancesWithErrorsAndTotalCzkBalance'

type Props = {
  allBalances: AllBalancesWithErrorsAndTotalCzkBalance,
  onAllBalanceRequest: typeof onAllBalancesRequest,
  loading: boolean
}

class BalancesScreen extends React.Component<Props> {
  constructor(props) {
    super(props)
    this.state = {
      loading: false,
    }
  }

  async componentDidMount() {
    this.loadBalances()
  }

  loadBalances = () => {
    this.props.onAllBalancesRequest()
  }

  renderItem = ({ item }) => (
    <Text style={styles.item}>
      <Text>
        {item.name}
        {' '}
      </Text>
      <Text>
        {numberFormatToShow(roundNumber(item.balance, 4), 8)}
        {' '}
      </Text>
      <Text>
        {numberFormatToShow(roundNumber(item.czkBalance, 4), 8)}
        {' '}
      </Text>
    </Text>
  )

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <TopStatusBar backgroundColor="#772ea2" barStyle="light-content" />
        <View>
          <Text style={balancesStyle.header}>Prostředky</Text>
          <Text style={balancesStyle.header}>
            Celkem
            {' '}
            {numberFormatToShow(roundNumber(this.props.allBalances.totalCzkBalance, 4), 8)}
            {' '}
Kč
          </Text>
          <FlatList
            data={this.props.allBalances.balances}
            renderItem={this.renderItem}
            keyExtractor={(item) => item.symbol}
            refreshControl={(
              <RefreshControl
                refreshing={this.state.loading}
                onRefresh={this.loadBalances}
              />
            )}
          />
        </View>
      </SafeAreaView>
    )
  }
}


const mapStateToProps = (state) => ({
  allBalances: state.allBalancesReducer.allBalances
})

const mapDispatchToProps = (dispatch) => bindActionCreators({ onAllBalancesRequest }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(BalancesScreen)
