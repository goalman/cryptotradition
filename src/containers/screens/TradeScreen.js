import { SafeAreaView, View, Text } from 'react-native'
import React from 'react'
import styles from '../../themes/index'
import TopStatusBar from '../../components/TopStatusBar'

export default class TradeScreen extends React.Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <TopStatusBar backgroundColor="#772ea2" barStyle="light-content" />
        <View>
          <Text>Trade Screen</Text>
        </View>
      </SafeAreaView>
    )
  }
}
