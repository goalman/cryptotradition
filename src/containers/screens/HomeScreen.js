import { SafeAreaView, View, Text } from 'react-native'
import React from 'react'
import styles from '../../themes/index'
import TopStatusBar from '../../components/TopStatusBar'

export default class HomeScreen extends React.Component {
  render() {
    return (
      <SafeAreaView style={styles.container}>
        <TopStatusBar backgroundColor="#772ea2" barStyle="light-content" />
        <View>
          <Text>Home Screen</Text>
        </View>
      </SafeAreaView>
    )
  }
}
